(function ($) {

	<!-- ######################### -->
	<!-- Verify if is a palindrome -->
	<!-- ######################### -->
	function isPalindrome(typedPalindrome){
		// Remove special chars and spaces
		var plainTypedPalindrome = typedPalindrome.replace(/[^A-Z0-9]/ig, "").toLowerCase();

		// Reverse
		var reversedPlainTypedPalindrome = plainTypedPalindrome.split('').reverse().join('');

		return plainTypedPalindrome === reversedPlainTypedPalindrome;
	}


	<!-- ######## -->
	<!-- DOM LOAD -->
	<!-- ######## -->
	$(document).ready(function() {
		// On submit form
		$('#formPalindrome').on('submit', function(evt) {
			evt.preventDefault();

			var typedPalindrome = $('#inputPalindrome').val();
			if(typedPalindrome.length < 1) {
				$('#resultPalindrome').removeClass('text-success');
				$('#resultPalindrome').addClass('text-error');
				$('#resultPalindrome').html('Type something!');

				return false;
			}

			var typedTextIsPalindrome = isPalindrome(typedPalindrome);

			if(typedTextIsPalindrome) {
				$('#resultPalindrome').removeClass('text-error');
				$('#resultPalindrome').addClass('text-success');
				$('#resultPalindrome').html('Is a palindrome');
			} else {
				$('#resultPalindrome').removeClass('text-success');
				$('#resultPalindrome').addClass('text-error');
				$('#resultPalindrome').html('Is not a palindrome');
			}

			return false;
		});
	});

}(jQuery));